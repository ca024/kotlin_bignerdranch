const val HERO_NAME = "Madrigal"

var playerLevel = 0
val hasBefriendedBarbarians = true
val hasAngeredBarbarians = false
val playerClass = "paladin"

fun main() {

    println("$HERO_NAME announces her presence to the world.")

    println("What level is $HERO_NAME?")
    // Non-null assertion operator or double-bang operator
//    val playerLevelInput = readLine()!!
//    playerLevel = if (playerLevelInput.matches("""\d+""".toRegex())) playerLevelInput.toInt() else 1

    // Refactor to remove non-null assertion operator
    playerLevel = readLine()?.toIntOrNull() ?: 0
    println("$HERO_NAME's level is $playerLevel.")

    readBountyBoard()

    println("Time passes...")
    println("$HERO_NAME returns from her quest.")

    playerLevel++
    println(playerLevel)
    readBountyBoard()
}

private fun readBountyBoard() {
    // Refactored to try/catch expression
    val message: String = try {
        val quest: String? = obtainQuest(playerLevel)

        quest?.replace("[Nn]ogartse".toRegex(), "XXXXXXXX")
            ?.let { censoredQuest ->
                """
            |$HERO_NAME approaches the bounty board. It reads:
            |   "$censoredQuest"   
            """.trimMargin()
            } ?: "$HERO_NAME approaches the bounty board, but it is blank."
    } catch (e: Exception) {
        "$HERO_NAME can't read what's on the bounty board."
    }

    println(message)

    // Handling exceptions using try/catch statement
//    try {
//        // Nullable variable
//        val quest: String? = obtainQuest(playerLevel)
//
//        // Using safe calls with let
//        val message: String? = quest?.replace("[Nn]ogartse".toRegex(), "XXXXXXXX")
//            ?.let { censoredQuest ->
//                """
//            |$HERO_NAME approaches the bounty board. It reads:
//            |   "$censoredQuest"
//            """.trimMargin()
//            }
//        // Using null coalescing operator or Elvis operator
//        println(message ?: "$HERO_NAME approaches the bounty board, but it is blank.")
//    } catch (e: Exception) {
//        println("$HERO_NAME can't read what's on the bounty board.")
//    }

    // Using the safe call operator
//    val censoredQuest: String? = quest?.replace("[Nn]ogartse".toRegex(), "XXXXXXXX")
//
//    if (censoredQuest != null) {
//
//        println(
//            """
//        |$HERO_NAME approaches the bounty board. It reads:
//        |   "$censoredQuest"
//        """.trimMargin()
//        )
//    }

    // Checking for null values with an if statement
//    if (quest != null) {
//        val censoredQuest: String = quest.replace("[Nn]ogartse".toRegex(), "XXXXXXXX")
//        println(
//            """
//        |$HERO_NAME approaches the bounty board. It reads:
//        |   "$censoredQuest"
//        """.trimMargin()
//        )
//    }


}

private fun obtainQuest(
    playerLevel: Int,
    playerClass: String = "paladin",
    hasBefriendedBarbarians: Boolean = true,
    hasAngeredBarbarians: Boolean = false
// Nullable return type
): String? {
//    if (playerLevel <= 0) {
//        // Throwing a Kotlin exception
//        throw IllegalArgumentException("The player's level must be at least 1.")
//    }

    // Refactor to precondition function
    require(playerLevel > 0) {
        // Throw custom exception
        throw InvalidPlayerLevelException()
    }
    // check, checkNotNull, requireNotNull, error, assert are precondition functions

    return when (playerLevel) {
        1 -> "Meet Mr. Bubbles in the land of soft things."
        in 2..5 -> {
            val canTalkToBarbarians = !hasAngeredBarbarians && (hasBefriendedBarbarians || playerClass == "barbarian")

            val barbarianQuest: String =
                if (canTalkToBarbarians) "Convince the Barbarians to call off their invasion."
                else "Save the town from the Barbarian invasions."

            barbarianQuest
        }
        6 -> "Locate the enchanted sword."
        7 -> "Recover the long-lost artifact of creation."
        8 -> "Defeat Nogartse, bringer of death and eater of worlds."
        else -> null
    }
}

// Defining custom exceptions
class InvalidPlayerLevelException() :
        IllegalArgumentException("Invalid player level (must be at least 1).")