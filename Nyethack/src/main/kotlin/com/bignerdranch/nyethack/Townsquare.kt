package com.bignerdranch.nyethack

// Defining a subclass
class Townsquare : Room("The Town Square") {

    override val status = "Bustling"
    private val bellSound = "GWONG"

    // Override a superclass function and make it final
    final override fun enterRoom() {
        narrate("The villagers rally and cheer as the hero enters")
        ringBell()
    }

    fun ringBell() {
        narrate("The bell tower announces the hero's presence: $bellSound")
    }
}