var heroName: String = ""

fun main() {
    heroName = promptHeroName()
    narrate("$heroName, ${createTitle(heroName)}, heads to the town square.")
    visitTavern()
}

private fun createTitle(name: String): String {
    return when {
        name.all { it.isDigit() } -> "The Identifiable"
        name.none { it.isLetter() } -> "The Witness Protection Number"
        name.count { it.lowercase() in "aieou" } > 4 -> "The Master of Vowels"
        else -> "The Renowned Hero"
    }
}

private fun makeYellow(message: String) = "\u001b[33;1m$message\u001b[0m"

private fun promptHeroName(): String {
    narrate("A hero enters the town of Kronstadt. What is their name?", ::makeYellow)
    println("Madrigal")
    return "Madrigal"
}