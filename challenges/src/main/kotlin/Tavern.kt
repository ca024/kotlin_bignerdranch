import java.io.File
import java.util.Collections

private const val TAVERN_MASTER = "Taernyl"
private const val TAVERN_NAME = "$TAVERN_MASTER's Folly"

private val firstNames = setOf("Alex", "Mordoc", "Sophie", "Tariq")
private val lastNames = setOf("Ironfoot", "Fernsworth", "Baggins", "Downstrider")

private val menuData = File("data/tavern-menu-data.txt")
    .readText()
    .split("\n")

private val menuItems = List(menuData.size) {
    index ->
    val (type, name, price) = menuData[index].split(",")
    listOf(type, name, price)
}

fun visitTavern() {
    narrate("$heroName enters $TAVERN_NAME")
    narrate("\n*** Welcome to Taernyl's Folly ***\n")

    // Challenge: Advanced Formatted Tavern Menu
    narrate(menuItems.joinToString(separator="\n") {
        it ->
        var menuTypes: MutableList<String> = mutableListOf()
        menuTypes.add(it[0])
        """
            ${" ".repeat(16 - it[0].length/2)}${if (Collections.frequency(menuTypes, it[0]) < 2) "~${it[0]}~" else ""}
            ${it[1]} ${".".repeat(31 - (it[1].length + it[2].length))} $${it[2]}
        """.trimIndent()

    })

    val patrons: MutableSet<String> = mutableSetOf()
    while (patrons.size < 10) {
        patrons += "${firstNames.random()} ${lastNames.random()}"
    }

    narrate("\n$heroName sees several patrons in the tavern:")
    narrate(patrons.joinToString())

//    repeat(3) {
//        placeOrder(patrons.random(), menuItems.random())
//    }
}

private fun placeOrder(
    patronName: String,
    menuItemName: String
) {
    narrate("$patronName speaks with $TAVERN_MASTER to place an order")
    narrate("$TAVERN_MASTER hands $patronName a $menuItemName")
}